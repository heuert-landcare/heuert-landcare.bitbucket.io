# FRAG - Factsheet Reports and Analysis Generator

## History

FRAG's history begins in the third quarter of 2018. The project was
initiated to innovate on reports, in particular for our geo-spatial
projects Our Environment and S-map Online. We wanted a tool that
would allow us to quickly create beautiful and functional reports
without having to re-invent the wheel for everything.

## Visualise Data with Re-usable Components

We had a few Agile sessions and sprints defining what we wanted.
It came up in these meetings that we might want to innovate on
something existing, rather than re-invent the wheel or do something
boring. Reports seem to be such and old concept with all
these existing server side report frameworks that just generate PDFs.
However, we wanted the reports to be interactive and work right in the
browser in our geo-spatial applications. So, you can include map
components, but it is possible to include anything that can be a web
component like thing.

So, we came up with the idea to re-use Observable.com's Notebooks
somehow. Observable Notebooks have a concept, that is a dependable
cell. Cells become observable in their own right, whether they are
generators, something that returns a promise, something that simply
returns a value or something that returns a document of various forms.

So, Observable cells are ideal for making them components of re-usable
reports or we think actually much more than that. While the reports
that can be generated with this work in a web application, we also
added features to make them printable. So, these cell collections
are re-usability on steriods. The Observable runtime creates the
dependency graph for us and we simply stich this together into
re-usable components that can be used in web applications.

Reports are generally not an exciting topic. We wanted to create something that would make it easy, flexible, interactive, powerful and fun to create dynamic reports on data.

The application we had in mind was a website with a powerful
geographic map page called Our Environment. Our Environment is our
innovation app, which S-map Online is based on. S-map Online is much
more highly used. There are 14,000 registered users on both services
but most of them come through S-map Online. Our Environment gets the
most innovative features though and for us is actually more
interesting. We provide rich reports on Our Environment with FRAG to
enable users to create informative reports about our Land in New
Zealand.

FRAG enables users to create responsive reports by using Observable Notebook cells that can contain report elements. It works very nicely with D3 or anything you can put into Observable Notebook cells.
FRAG is a concept built on Observable Notebooks.
