# Tim-Hinnerk Heuer - Bio

I'm a web developer with a passion for geospatial web applications and a vetted interest in new technology. I love mathematics and algorithms and spend a lot of my time learning new things. I am interested in frameworks, design patterns and of course anything to do with mapping applications. As a geo-web developer I have spent a lot of time on geospatial web application and have contributed to FOSS4G as much as I could.

Please check out our web applications:

[Our Environment](https://ourenvironment.scinfo.org.nz/)
and
[S-map Online](https://smap.landcareresearch.co.nz/)

Please don't hesitate to get in touch:
[@geekdenz](https://twitter.com/geekdenz)