```javascript
md`# Einstein
${e}
`
// *****
e = tex`e=mc^2`
// *****
function f(time) {
  return `CLOCK: ${time}`
}
// *****
g = {
  let i = 1
  while (true) yield f(new Date)
}
```
Show globe Notebook and explain.

Then show

```javascript
md`# Global Earthquakes`
// *****
import {globe} with {r as rotation} from '@sinclarius/earthquake-globe'
// *****
r = 180
// *****
r = {
  while (true) yield Promises.tick(1000/30, Date.now()/20)
}
```

- Show Hillshade and import
- Show Table, Bar Chart, Bubble Chart.
- Fork, Publish and Import Bubble Chart
- Show OE Layer=Topic, Sections=Pre-human, current wetlands

```javascript
import {h} from '@denz/hillshader'
// *****
r = 180
// *****
import {h} with {r as rotation} from '@denz/hillshader'
// *****
r = {
  while (true) yield Promises.tick(1000/30, Date.now()/20)
}
```