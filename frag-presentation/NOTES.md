# Design Notes

FRAG is a dependency injection model for reports. Actually, it is more than that.
It is a framework for making any observable notebook re-usable.
Observable HQ is a web application/framework by the D3 developer(s) that enables
developers to create dynamic notebooks in JavaScript.
Each notebook has observable cells that can return a promise of a value or
an observable value that can dynamically change over time and have the
depending cells update instantly and with minimal resource consumption.
The concept of Observable HQ is somewhat simple, but very powerful.
A user can edit a cell which can be JavaScript code, but also HTML, MarkDown 
or in principle anything that is visual.
There is a LateX plugin for Observable HQ and apart from
MathML in HTML, there are now many ways also to express mathematical
formulae on the web with ease.
Interactive notebooks have been used to visualise things ranging from
moebius strips in 3D, interactive vector maps and papers with formulae
and descriptive text to fully fletched 3D engines written in standard
SQL and dynamic, interactive data visualisation with D3 or similar
libraries. Anything that is on the front-end and can be expressed with
code or markup/down can be made re-usable with Observable (HQ). It is
a great platform that has spawned a lot of interest in the developer
community and makes designing interactive visualisation fun and quick.

After careful consideration at the beginning of the FRAG project,
we could have come up with a boring old reports generator, but we
thought, our job as software engineers is to create platforms that
enable others create great things with ease, enjoyment and productivity.

Also, we needed a project that is fun and inspiring for us, the people
developing this as well as delighting the client with a product
that can benefit scientists in a flexible and productive way.

So, we are designing FRAG:

FRAG is simply an extension of Observable HQ in that it makes notebooks
and smaller units such as cells re-usable by plugging in different
data. D3 stands for Data Driven Documents. But remember Observable HQ is
not "just" that.

FRAG enables developers to plug web application data into reports that are
driven by observable notebooks. Each notebook gets saved every time the
user makes a change. FRAG simply stores a notebook and/or cells' state within
labeled or categorised or otherwise sorted containers or units.
These containers or units then make up a re-usable web component that can
be injected into a web application or web view on mobile. It is dependency
injection for reports.

Let me briefly explain dependency injection, because it is not a trivial
concept:
Units can be functions, classes or any functional components in software.
Targets, in the following are simply units that other units depend on.
Dependency Injection or DI takes units and the targets they
depend on and injects the targets into the depending units instead of directly
depending on them. This can be done for example by only depending on a
function's signature, its return and parameter types and then have a
configuration inject them with a unit setter function.
In software development's Test Driven Development, this has the advantage
of allowing unit testing.

As a brief explanation: Dependency Injection enables maximum re-usability
in code and also testability by creating small units and
having the dependencies injected rather than just referenced. For example
if a unit is considered to be function, we only care about its signature
and the signature is what our unit depends on and NOT the implementation.
This is more easily testable, because a fake function can be injected
into the piece of code to test and the real implementation that is used
does not run. This way, in testing it can be asserted that the function
does what it says in its contract, the function signature. This concept
extends to classes in object oriented programming where interfaces are
used to define the contracts between unit and target.

The other big advantage of DI is re-usability is optimised. If we always
use the smallest possible unit and build larger units using smaller
units, we can re-use them on any level, making the components or targets
re-usable in interesting ways.

So, FRAG does just this, but with a higher level concept, a cell of functionality.
A cell in an observable notebook has dependencies that are other
cells. We "simply" inject our own data into cells in an observable notebook
and put together reports like this.
This way, a developer can create the nitty gritty details of a report,
define how it actually looks and behaves, how it processes the data that is
passed in and a user, which is normally another developer, can then inject
their data into a cell and compose a collection cells to form a custom
report for their application.

This concept could be taken further, to a level where users do not have
to be developers, but end users, that potentially click together a report
that they design. They then don't have to think about the detail of "How
do I create a bar chart?" or "How do I create a tree map for my data?".

They could simply browse or search the available reports or even notebooks and
find a report or cell(s) that they want to use in their presentation, plug in
their data source by defining the structure and construct a report much more
quickly and visually pleasing than has been possible so far.

FRAG is like lego for reports.

## Advanced Examples of Observable Notebooks

Hillshader with hypsometric tint and textured layer maps:
https://observablehq.com/@sahilchinoy/hillshader

This an example notebook with cells that have an image as a data source.
We could for exmample make a WMS the input image to this, which would
mean the "static" map would become a dynamically generated image component
that end-users could appreciate and use to understand underlying data more
accurately.

Tom and I have been modifying it to show what sort of thing could be done
with FRAG. See here:
https://observablehq.com/d/44805be5e3dd8f75
for an example.
Here we replace the DEM from the notebook with the DEM layer from LRIS
and do an animation with it. This could be used to visualise the
movement of the sun using hypsometric tinting for example. We simply
replace the azimuth or aspect of the hillshade with an arbitrary value
between 0 and 360&#176; bound to the time value to determine the speed.
Here the input could be the image URL, which could be dynamically
generated within an app and passed to the report component.

A report component, on a high level could be a custom HTML element.
For example the following:

```html
<HillShader
    title="Hello hill"
    tint="10.2" <!-- some unit -->
>
    <Image src="//somewms.url/wms?service=WMS&request=GetMap&width=1080&height=1920&..." />
</HillShader>
```

These HTML elements can be re-used with other parameters and plugged into
an existing application by loading a bootstrap library for the reports and
pulling in the dependencies from FRAG which would have cached the Observable HQ
notebook's cells and code.

## UI Vision

To get this off the ground, we want to develop a WYSIWYG editor for this to make
it much easier for scientists with minimal or no coding knowledge to edit these
reports with some ease.

For this we need to do further investigation as to how we can make an intuitive
UI for people.

However, before we do the UI, I think a good foundation is to solidify creating
components with custom HTML 5 elements called web components. Angular makes great
use of web components, so we chose this as the technology for our front-end
design.

The backend needs to be a service that can handle caching the observable notebooks
and also handle the code management.

With Observable HQ you can import notebook data into your own notebooks. You can
replace function parameters with your own with a special syntax:

```javascript
import {drawHillshade, elevation, drawMap, colorData} with {azimuth, width, height, url as molokai} from "@sahilchinoy/hillshader"
```

In this example we import the properties drawHillshade, elevation, drawMap, colorData
from the notebook "@sahilchinoy/hillshader". Here the injected cells with names
drawHillshade, elevation, drawMap, colorData are replaced with local cells from
this notebook (azimuth, width, height, url as molokai).

This could be formally defined with BNF as

```javascript
import {<cells>} with {<cells>} from "<notebook>"

<cells>    ::= <cell>[, <cells>]
<cell>     ::= JAVASCRIPT_IDENTIFIER
<notebook> ::= @CSS_IDENTIFIER/CSS_IDENTIFIER
```

